import java.util.*;
public class Gravitacija{
    public static void main(String[]args){
        Scanner input = new Scanner(System.in);
        double nadV = input.nextInt();
        izpis(nadV,izracunGravitacije(nadV));
        
    }
    
    public static double izracunGravitacije (double nadVisina) {
        long CM = 398571280;
        long R = 6371000;
        
        double gravitacija = (CM * 1000000) / ((R + nadVisina) * (R + nadVisina));
        return gravitacija;
    }
    
    public static void izpis(double nadV, double gravP) {
        System.out.printf("Nadmorska višina: %f %n", nadV);
        System.out.printf("Gravitacijski pospešek: %f", gravP);
    }
}